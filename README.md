Integration of Multiplexed Imaging and Other Single Cell Assays
===============================================================

To do
=====

-   Write some background notes
-   Write some methodological notes
-   Run simulations based on the notes
-   Define quality metrics
-   Test on real data
-   Other stuff

Installation
============

This will install the project functions and vignette from
[Bitbucket](https://bitbucket.com/simonvandekar/). These will include
the published function from the package, but the README will have
everything I am typing here and vignettes might have more details about
how to run the functions developed as part of this project on a new data
set.

    devtools::install_bitbucket("simonvandekar/scIRI")

Background notes
================

Possible correspondence types between data sets
-----------------------------------------------

Data are collected from different modalities, one of which is imaging
data. Assume the other is scRNA-seq for now. We want to learn (in the
human sense) from the spatial aspects of the multiplexed imaging data
and the high-dimensional aspects of the scRNA-seq. Data cannot be
collected from the exact same sample from both modalities. scRNA-seq and
The correspondence between data sets can be (at least) any of the
following

1.  Imaging and scRNA-seq data are from different tissues with similar
    cell populations. E.g. different samples of similar tissue types.
2.  Imaging and scRNA-seq data are from completely different cell
    populations, but the cell classes are known.
3.  Imaging and scRNA-seq data are from completely different cell
    populations and the cell classes are unknown.

The correspondence between measurements taken on each data set can be
(at least) any of the following

<ol type="a">
<li>
The exact same type measurement in both modalities.
</li>
<li>
Similar, but different measurements in both modalities.
</li>
<li>
Different and not similar measurements in both modalities.
</li>
</ol>
Methodological Approach
=======================

Homogeneous Cell Populations
----------------------------

### Notation

Assume *Y*<sub>*m*, *c*, 0</sub> ∈ ℝ<sup>*p*<sub>0</sub></sup> and
*Y*<sub>*m*, *c*, 1</sub> ∈ ℝ<sup>*p*<sub>*m*</sub></sup> are marker
measurements taken for the data modality *m* = 1, …, *M*, for cell
*c* = 1, …, *C*<sub>*m*</sub> (assuming =fully processed datasets). Let
*X*<sub>*m*, *c*, 0</sub> ∈ ℝ<sup>*q*<sub>0</sub></sup> denote
covariates taken for all modalities and
*X*<sub>*m*, *c*, 1</sub> ∈ ℝ<sup>*q*<sub>*m*</sub></sup> denote
covariates taken only for modality *m*.

Assume components of the population are normally distributed. 1.a,c
implies
*Y*<sub>*m*, *c*</sub> ∼ *N*{*μ*(*X*<sub>*m*, *c*, 0</sub>), *Σ*<sub>*m*</sub>},
where
$$
\\Sigma\_m= \\begin{bmatrix}
\\Sigma\_0 & \\Sigma\_{m,01} \\\\
\\Sigma\_{m,10} & \\Sigma\_{m,11}
\\end{bmatrix}
$$

1.b,c implies
*Y*<sub>*m*, *c*</sub> ∼ *N*{*μ*<sub>*m*</sub>(*X*<sub>*m*, *c*</sub>), *Σ*<sub>*m*</sub>},
where
$$
\\Sigma\_m= \\begin{bmatrix}
\\Sigma\_{m,0} & \\Sigma\_{m,01} \\\\
\\Sigma\_{m,10} & \\Sigma\_{m,11}
\\end{bmatrix}
$$
 and
*Σ*<sub>*m*, 0</sub> = *ζ*<sub>*m*</sub>*P**ζ*<sub>*m*</sub><sup>*T*</sup> + *Σ*<sub>*m*, *E*</sub>
 where *ζ*<sub>*m*</sub> ∈ ℝ<sup>*p*<sub>0</sub> × *r*</sup> is a
diagonal matrix, *P* is a covariance matrix, and *Σ*<sub>*m*, *E*</sub>
is a covariance matrix.

Similar populations – nonparametric.

### Estimation

The covariance matrix *P* represents the dependence of latent variables
that account for signal in the two datasets. The choice of distribution
of the latent variables implies a particular signal structure that is
shared across datasets and any nonnormal latent variable implies that
the distributional assumptions are only an approximation.

Here, we consider the latent variable model

$$
\\begin{aligned}
Y\_{m,c,0} & = \\alpha\_m W\_{m,c} + \\beta\_m Z\_{m,c} +E\_{m,c}\\\\
& = \\zeta\_m V\_{m,c} + E\_{m,c},
\\end{aligned}
$$
 where *α*<sub>*m*</sub> ∈ ℝ<sup>*p*<sub>0</sub> × *t*</sup> is an
unconstrained matrix and and
*β*<sub>*m*</sub> ∈ ℝ<sup>*p*<sub>0</sub> × *p*<sub>0</sub></sup> is a
diagonal matrix,
*ζ*<sub>*m*</sub> = \[*α*<sub>*m*</sub> *β*<sub>*m*</sub>\],
*V*<sub>*m*, *c*</sub> = \[*W*<sub>*m*, *c*</sub><sup>*T*</sup> *Z*<sub>*m*, *c*</sub><sup>*T*</sup>\]<sup>*T*</sup>,
*W*<sub>*m*, *c*</sub> is multinomial with probability parameter
*π* ∈ \[0, 1\]<sup>*t*</sup> and *Z*<sub>*m*, *c*</sub> is a variance
one, multivariate normal random variable with correlation matrix
*Σ*<sub>*Z*</sub>. Here, *W*<sub>*m*, *c*</sub> denotes a latent vector
of *t* the cell types for cell *c* measured in dataset *m* and
*Z*<sub>*m*, *c*</sub> is a latent measurement corresponding to cell *c*
in dataset *m* that captures the shared signal across the datasets.
Obtaining predictions of *Z*<sub>*m*, *c*</sub> yields a normalization
that accounts for the joint distribution of measurements from the two
datasets. We use the Expectation-Maximization (EM) algorithm to estimate
parameters in this model.

#### Without cell classes (i.e. *α*<sub>*m*</sub> = 0)

This approach can be used for normalization if classification is going
to be performed using another algorithm afterward. In This case,
*W*<sub>*m*, *c*</sub> = 0 and the complete data log-likelihood is
$$
\\begin{aligned}
\\ell(\\theta; Y) = & -\\frac{1}{2} \\sum\_{m,c} \\Bigl\\{ (y\_{m,c} - \\beta\_m Z\_{m,c})^T \\Sigma\_{m,E}^{-1}(y\_{m,c} - \\beta\_m Z\_{m,c}) + \\log\\lvert\\Sigma\_{m,E}\\rvert \\\\
& +Z\_{m,c}^T \\Sigma\_Z^{-1} Z\_{m,c} + \\log\\lvert\\Sigma\_{Z}\\rvert \\Bigr\\}.
\\end{aligned}
$$
 The expectation of ℓ given parameter estimates *θ*<sup>(*t*)</sup> and
*Y* is
$$
\\begin{aligned}
\\mathbb{E}\\{ \\ell \\mid Y, \\theta^{(t)}\\} = & -\\frac{1}{2} \\sum\_{m} N\_m \\Big\\{ \\mathrm{tr}\\{\\Sigma^{-1}\_{m,E} C\_{m} \\} + \\mathrm{tr}\\{\\Sigma\_{m,E}^{-1}\\beta\_m \\left \[ \\delta\_m^{(t)T}C\_{m}\\delta\_m^{(t)} + \\mathrm{Cov}(Z\\mid Y\_m)^{(t)} \\right\] \\beta\_m^T\\} \\\\
& -2 \\mathrm{tr}\\{ \\Sigma\_{m,E}^{-1}\\beta\_m \\delta^{(t)}\_m C\_{m}\\} + \\mathrm{tr}\\{\\Sigma\_Z^{-1} \\left \[ \\delta\_m^{(t)T}C\_{m}\\delta\_m^{(t)} + \\mathrm{Cov}(Z\\mid Y\_m)^{(t)} \\right\] \\} + \\log\\lvert \\Sigma\_{m,E} \\rvert+ \\log\\lvert \\Sigma\_Z \\rvert \\Big\\},
\\end{aligned}
$$
 where
$$
\\begin{aligned}
\\delta^{(t)}\_m & =  \\Sigma\_Z^{(t)} \\beta\_m^{(t)T}(\\beta\_m^{(t)} \\Sigma\_Z^{(t)} \\beta\_m^{(t)T} + \\Sigma\_{m,E}^{(t)})^{-1}  \\\\
\\mathrm{Cov}(Z\\mid Y\_m)^{(t)} & = \\Sigma\_Z^{(t)} - \\Sigma\_Z^{(t)} \\beta\_m^{(t)T}(\\beta\_m^{(t)} \\Sigma\_Z^{(t)} \\beta\_m^{(t)T} + \\Sigma\_{m,E}^{(t)})^{-1} \\beta\_m^{(t)} \\Sigma\_Z^{(t)} \\\\
C\_m & = N\_m^{-1}\\sum\_{c} Y\_{m,c} Y\_{m,c}^T
\\end{aligned}
$$

Differentiating and solving for parameters gives
$$
\\begin{aligned}
\\hat\\beta\_{m} & = \\mathrm{root}\_{\\beta\_{m}} \\mathrm{diag}\\Bigl\\{ N\_{m}\\left \[ \\delta\_m^{(t)T}C\_{m}\\delta\_m^{(t)} + \\mathrm{Cov}(Z\\mid Y\_m)^{(t)} \\right\] \\beta\_m^T \\Sigma\_{m,E}^{-1} - \\sum\_{c}^{N\_m} \\delta^{(t)}\_m Y\_{m,c} Y\_{m,c}^T \\Sigma^{-1}\_{m,E}\\Bigr\\}\\\\
\\hat \\Sigma\_{m,E} & = N^{-1}\_m \\sum\_{c=1}^{N\_m} Y\_{m,c} Y\_{m,c}^T + \\beta\_m \\left \[ \\delta\_m^{(t)T}C\_{m}\\delta\_m^{(t)} + \\mathrm{Cov}(Z\\mid Y\_m)^{(t)} \\right\]\\beta\_m^T - Y\_{m,c} Y\_{m,c}^T \\delta^{(t)T}\_m \\beta\_m^T - \\beta\_m \\delta^{(t)}\_mY\_{m,c} Y\_{m,c}^T  \\\\
\\hat\\Sigma\_Z & = M^{-1}\\Bigl\\{ \\sum\_{m=1}^M\\mathrm{Cov}(Z\\mid Y\_m)^{(t)} + \\sum\_{c}^{N\_m} \\delta^{(t)T}\_mY\_{m,c} Y\_{m,c}^T \\delta^{(t)}\_m \\Bigr\\}
\\end{aligned}
$$

#### With cell classes

We use the Expectation-Maximization algorithm to estimate the parameters
and obtain predictions of the latent variables. The complete data
log-likelihood is
$$
\\begin{aligned}
\\ell(\\theta; Y) = & -\\frac{1}{2} \\sum\_{m,c} \\Bigl\\{ (y\_{m,c} - \\zeta\_m V\_{m,c})^T \\Sigma\_{m,E}^{-1}(y\_{m,c} - \\zeta\_mV\_{m,c}) + \\log\\lvert\\Sigma\_{m,E}\\rvert \\\\
& +Z\_{m,c}^T \\Sigma\_Z Z\_{m,c} + \\log\\lvert\\Sigma\_{Z}\\rvert \\\\
&  - 2\\sum\_{k=1}^t W\_{m,c}^k \\log \\pi^k \\Bigr\\}.
\\end{aligned}
$$
 Differentiating and solving for parameters conditional on random
variables gives
$$
\\begin{aligned}
\\hat \\zeta\_{m} = (V\_{m}^T \\Sigma\_{m,E}^{-1} V\_{m})^{-1} V\_m^T \\Sigma\_{m,E}^{-1} Y\_m \\\\
\\end{aligned}
$$

### Evaluating in a simulated dataset

Mostly not run. Was exploring different methods to estimate the model
above. The model is not estimable because the residual matrix
*Σ*<sub>*m*, *E*</sub> is not positive definite. I think to estimate a
positive definite *Σ*<sub>*m*, *E*</sub>, you have to restrict the range
on $\\Beta$. Might be interesting to try this.

Only this first chunk of code is run.

    library(mvtnorm)
    library(scIRI)
    set.seed(100)
    nsim = 1000
    # number of modalities
    M = 2
    # sample size
    ns = c(100, 500)

    # number of shared variables
    p0 = 10
    # number of variables that are not shared for each modality
    #p1 = rep(50, length(M))

    # mean parameters

    # covariance parameters
    # this is the distribution of the latent variable Z
    rho = 0.6
    SigmaZ = rho^(abs(outer(1:p0, 1:p0, '-')) )
    # 
    Sigmas = list(NA)
    Sigmas = rep(Sigmas, M)
    betas = list(NA)
    betas = rep(betas, M)
    for(m in 1:M){
      #
      Sigmas[[m]] = diag(rgamma(p0, shape = m, scale = 1))
      betas[[m]] = diag(rnorm(p0, sd = 1+M-m))
    }

    # covariates
    # ignore for now
    Y = mapply(function(Sigma, beta, n, SigmaZ){
      rmvnorm(n, sigma=SigmaZ) %*% beta + rmvnorm(n, sigma=Sigma)
    }, Sigma=Sigmas, beta=betas, n=ns, MoreArgs=list('SigmaZ'=SigmaZ), SIMPLIFY = FALSE )

    results = scIRI::covModel(Y)

    debug(estimate)
    Beta = list(rep(1, p0))
    Beta = rep(Beta, M)
    # not converging evaluate log-lik at true values to see if it is higher than the estimates?
    test = estimate(Y, Beta = Beta, maxit = 300)
    debug(beta_mSigma_mEest)
    beta_mSigma_mEest(covY = CovY[[1]], E=E[[1]], sigma_mE = Sigma_mE[[1]])
    beta_mSigma_mEest(covY = CovY[[1]], sigma_z = E[[1]]$covZmidY, delta=E[[1]]$delta, sigma_mE = Sigma_mE[[1]])

    # latent variable estimation of this model
    library(lavaan)
    data("HolzingerSwineford1939")
    model = paste(paste0('v', 1:9), paste0('x', 1:9), sep='=~', collapse='; ')
    exmodel = ' visual =~ x1 + x2 + x3;
    textual =~ x4 + x5 + x6; speed =~ x7
    + x8 + x9 '

    extest = cfa(exmodel, data=HolzingerSwineford1939, group='school', group.equal=c('lv.variances', 'lv.covariances'))
    testgrp = cfa(model, data=HolzingerSwineford1939, group='school', group.equal=c('lv.variances', 'lv.covariances'), std.lv=TRUE)
    test = cfa(model, data=HolzingerSwineford1939)

    cors = simplify2array(by(HolzingerSwineford1939[,paste0('x', 1:9)], HolzingerSwineford1939[,'school'], cor))
    meancor = rowMeans(cors, dims=2)
    covs = simplify2array(by(HolzingerSwineford1939[,paste0('x', 1:9)], HolzingerSwineford1939[,'school'], cov))
    meancov = rowMeans(covs, dims=2)
    sweep(covs, 1:2, meancov, FUN='-')

    resid(testgrp)
    # latent variable correlation is effectively the average of the correlation matrices
    #summary(testgrp)

### 

Heterogeneous cell populations
------------------------------

Cell populations may differ between modality due to samples from
different populations of cells where the classes are known within each
modality. For example, proportions of tumor and healthy tissue/cells may
be ascertained using a tissue segmentation algorithm or classification
algorithm.

### With known cell classes

### Matching cell populations between modalities

### Tests for similarities between cell populations
